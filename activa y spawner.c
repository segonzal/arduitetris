// Pins de la pantalla led
int latchPin = 11;
int clockPin = 13;
int dataPin = 12;

// Variables globales
char screen[16]; //arreglo de filas
byte posX, posY, nvl;
int fils, active, pntj;
//O I S Z L J T
int pzas[] = {1632,240,1728,3168,736,2272,1248};

//Rota la pieza si es posible. 
void Rotar(){
	byte pieza = (getType_Orientation()) && B00000111;
	byte orientacion = (getType_Orientation())>>3;
	switch(pieza){
		case 0:
			return;
		case 1:
			if(orientacion==0 && posY!=0 && posY<14 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY+2],posX)!=1!=1 && bitRead(screen[posY-1],posX)!=1){
				generalRotation();
			}
			else if(orientacion==1 && posX>1 && posX<7 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY],posX-2)!=1){
				generalRotation();
				active=active>>1;
				return;
			}
		case 2:
			if(orientacion==0 && posY>0 && bitRead(screen[posY-1],posX)!=1 && bitRead(screen[posY+1],posX+1)!=1){
				generalRotation();
				return;
			}
			else if(orientacion==1 && posX>0 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY+1],posX-1)!=1){
				generalRotation();
				active=active<<4;
				return;
			}
		case 3:
			if(orientacion==0 && posY>0 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY-1],posX+1)!=1){
				generalRotation();
				return;
			}
			else if(orientacion==1 && posX>0 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY+1],posX+1)!=1){
				generalRotation();
				active=active<<4;
				return;
			}
		case 4:
			if(orientacion==0 && posY>0 && bitRead(screen[posY-1],posX)!=1 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY+1],posX+1)!=1 || 
			orientacion==1 && posX>0 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY-1],posX+1)!=1 || 
			orientacion==2 && posY<15 && bitRead(screen[posY-1],posX)!=1 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY-1],posX-1)!=1 || 
			orientacion==3 && posX<7 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY+1],posX-1)!=1){
				generalRotation();
				return;
			}
		case 5:
			if(orientacion=0 && posY>0 && bitRead(screen[posY-1],posX)!=1 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY-1],posX+1)!=1 || 
			orientacion==1 && posX>0 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY-1],posX-1)!=1 || 
			orientacion==2 && posY<15 && bitRead(screen[posY-1],posX)!=1 && bitRead(screen[posY+1],posX)!=1 && bitRead(screen[posY+1],posX-1)!=1 || 
			orientacion==3 && posX<7 && bitRead(screen[posY],posX+1)!=1 && bitRead(screen[posY],posX-1)!=1 && bitRead(screen[posY+1],posX+1)!=1){
				generalRotation();
				return;
			}
		case 6:
			if(orientacion==0 && posY>0 && bitRead(screen[posY-1],posX)!=1 || 
			orientacion==1 && posX>0 && bitRead(screen[posY],posX-1)!=1 || 
			orientacion==2 && posY<15 && bitRead(screen[posY+1],posX)!=1 || 
			orientacion==3 && posX<7 && bitRead(screen[posY],posX+1)!=1){
				generalRotation();
				return;
			}
		}
	}

//Mueve hacia la derecha si es posible. Si es posible, suma a posX un 1 (mover a la derecha). 
//Si no, no hace cambios.
void moverD(){
	byte pieza = (getType_Orientation())>>5;
	byte orientacion = (getType_Orientation()) && B00000011;
	switch(pieza){
		case 0:
			if(posX<7){
				generalMove(0);
			}
		case 1:
			if(orientacion == 0 && posX<6 || orientacion == 1 && posX<7){
				generalMove(0);
			}
		case 2:
			if(posX<6){
				generalMove(0);
			}
		case 3:
			if(posX<6){
				generalMove(0);
			}
		case 4:
			if(orientacion < 3 && posX<6 || orientacion == 3 && posX<7){
				generalMove(0);
			}
		case 5:
			if(orientacion < 3 && posX<6 || orientacion == 3 && posX<7){
				generalMove(0);
			}
		case 6:
			if(orientacion < 3 && posX<6 || orientacion == 3 && posX<7){
				generalMove(0);
			}
	}
}

//Mueve hacia la izquierda si es posible. Si es posible, resta a posX un 1 (mover a la izquierda). 
//Si no, no hace ningun cambio.
void moverI(){
	byte pieza = (getType_Orientation()) && B00000111;
	byte orientacion = (getType_Orientation())>>3;
	switch(pieza){
		case 0:
			if(posX>1){
				generalMove(1);
			}
		case 1:
			if(orientacion == 0 && posX>2 || orientacion == 1 && posX>0){
				generalMove(1);
			}
		case 2:
			if(orientacion == 0 && posX>1 || orientacion == 1 && posX>0){
				generalMove(1);
			}
		case 3:
			if(orientacion == 0 && posX>1 || orientacion == 1 && posX>0){
				generalMove(1);
			}
		case 4:
			if(orientacion == 0 && posX>1 || orientacion == 1 && posX>0 || orientacion > 1 && posX>1){
				generalMove(1);
			}
		case 5:
			if(orientacion == 0 && posX>1 || orientacion == 1 && posX>0 || orientacion > 1 && posX>1){
				generalMove(1);
			}
		case 6:
			if(orientacion == 0 && posX>1 || orientacion == 1 && posX>0 || orientacion > 1 && posX>1){
				generalMove(1);
			}
	}
}

//Suma un una a posY (cae un paso) mientras no caiga en un estado estatico.
void caerForzado(){
	while(!(esEstatico())){
		posY+=1;
	}
}

//Retorna un 1 si la pieza no puede moverse mas hacia abajo. Un 0 encaso contrario.
boolean esEstatico(){
	int i=0, j=0;
	for(i; i<4; i++){
		for(j; j<4; i++){
			if(bitRead(active, 4*i + j) && (posY+i>15) && bitRead(screen[posY+i],posX+j-2)!=1){
				return true;
			}
		}
	}
	return false;
}

//Funcion que recibe un flag y de acuerdo a este, mueve la pieza hacia la derecha, izquierda o hacia abajo en un paso.
//Si recibe un uno, mueve la pieza hacia la izquierda si es posible. Si recibe un cero, mueve la pieza hacia la derecha si es posible.
//Si recibe un dos, mueve la pieza hacia abajo si es posible.
void generalMove(int k){
	int i=0, j=0;
	if(k==0){
		for(i; i<4; i++){
			for(j; j<4; i++){
				if(bitRead(active, 4*i + j) && (posX+j-1>7) && bitRead(screen[posY+i-1],posX+j-1)!=1){
					return;
				}
			}
		}
		posX+=1;
		return;
	}
	if(k==1){
		for(i; i<4; i++){
			for(j; j<4; i++){
				if(bitRead(active, 4*i + j) && (posX+j-3<0) && bitRead(screen[posY+i-1],posX+j-3)!=1){
					return;
				}
			}
		}
		posX-=1;
		return;
	}
	if(k==2 && !(esEstatico())){
		posY+=1;
		return;
	}
	return;
}

//Suma un uno a posY si es posible.
void caer(){
	generalMove(2);
	return;
}

void generalRotation(){
	int base= 0;
	byte i=0, j=0;
	int x,y;
	for(j; j<4; j++){
		for(i=0; i<4; i++){
			if(bitRead(active, i+4*j)==1){
				x=j+1;
				y=3-i;
				bitWrite(base, x+4*y,1);
			}
		}
	}
	active=base;
	return;
}

//Retorna el tipo y orientacion de la pieza activa.
//Los primeros 3 bits corresponden al tipo, los 2 bits siguientes corresponden a la orientacion.
byte getType_Orientation(){
	if(active == 1632){
		return B00000000;
	}
	else if(active == 240){
		return B00000001;
	}
	else if(active == 8738){
		return B00001001;
	}
	else if(active == 1728){
		return B00000010;
	}
	else if(active == 2240){
		return B00001010;
	}
	else if(active == 3168){
		return B00000011;
	}
	else if(active == 1224){
		return B00001011;
	}
	else if(active == 736){
		return B00000100;
	}
	else if(active == 3140){
		return B00001100;
	}
	else if(active == 232){
		return B00010100;
	}
	else if(active == 1094){
		return B00011100;
	}
	else if(active == 2272){
		return B00000101;
	}
	else if(active == 1100){
		return B00001101;
	}
	else if(active == 226){
		return B00010101;
	}
	else if(active == 1604){
		return B00011101;
	}
	else if(active == 1248){
		return B00000110;
	}
	else if(active == 1220){
		return B00001110;
	}
	else if(active == 228){
		return B00010110;
	}
	else if(active == 1124){
		return B00011110;
	}
}



///////////////////////
///Prototipo Spawner///
///////////////////////

//Genera una pieza aleatoria en el spawner. Si la pieza coliciona con una pieza estatica 
//en el spawner (continuar()=falso) resetea el juego y se vuelve a llamar la funcion.
//Si no, solo retorna.
void generarPieza(){
	int i = random(0,7);
	active = pzas[i];
	posX=4;
	posY=6;
	if(continuar()){
		return;
	}
	else{
		reset();
		generarPieza();
	}
}

//setea a 0 el nivel, puntaje y los valores de la pieza estatica
void reset(){
	byte i=0;
	nvl = pntj = 0;
	while(i<16){
		screen[i]=0;
	}
}

//Retorna verdadero si la pieza spawneada no coliciona con la pieza estatica. En caso contrario, retorna falso
boolean continuar(){
	switch(active){
		case 1632:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX-1) || bitRead(screen[1],posX) || bitRead(screen[1],posX-1)){
				return false;
			}
		case 240:
			if(bitRead(screen[0],posX-2) || bitRead(screen[0],posX-1) || bitRead(screen[1],posX) || bitRead(screen[1],posX+1)){
				return false;
			}
		case 1728:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX+1) || bitRead(screen[1],posX) || bitRead(screen[1],posX-1)){
				return false;
			}
		case 3168:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX-1) || bitRead(screen[1],posX) || bitRead(screen[1],posX+1)){
				return false;
			}
		case 736:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX-1) || bitRead(screen[0],posX+1) || bitRead(screen[1],posX-1)){
				return false;
			}
		case 2272:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX-1) || bitRead(screen[0],posX+1) || bitRead(screen[1],posX+1)){
				return false;
			}
		case 1248:
			if(bitRead(screen[0],posX) || bitRead(screen[0],posX-1) || bitRead(screen[0],posX+1) || bitRead(screen[1],posX)){
				return false;
			}
	}
	return true;
}
