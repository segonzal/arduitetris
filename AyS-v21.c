// Pins de la pantalla led
int latchPin = 11;
int clockPin = 13 ;
int dataPin = 12;

//refreshing screen time
unsigned long time;

// Variables globales
byte posX, posY, nvl;
int active, pntj, lineas;
//O I S Z L J T
int pzas[] = {1632,240,1728,3168,736,2272,1248};
//pantalla
char screen[16];

//Thread safe.
void rotar(){
  int newactive = 0;
  switch(active){
    case 1632:
      return;
    case 0x08c4:
      newactive=1728;
    case 0x04c8:
      newactive=3168;
    default:
      int j=2;
      for (int i=0;i<16;i++){
        boolean value = (active & (1<<i))!=0;
        int index = 15 - (4*(i%4)+j);
        bitWrite(newactive,index,value);
        if(i==3 || i==7) j--;
        if(i==11) j=3;
      }
  }
  if(!colisiona(newactive, posX, posY)){
    active=newactive;
  }
}

//Thread safe.
boolean colisiona(int pieza, byte x, byte y){
  int i=0, j=0;
  for(i; i<4; i++){
      for(j; j<4; j++){
        if(bitRead(pieza, 4*i + j)==1 && (y+i-1<0) && (y+i-1>15) && (x+j-2<0) && (x+j-2>7) && bitRead(screen[y+i-1],x+j-2)==1){
          return true;
        }
      }
    }
  return false;
}

//Thread safe.
void moverD(){
  if(!colisiona(active, posX+1, posY)){
    posX+=1;
    return;
  }
}

//Thread safe.
void moverI(){
  if(!colisiona(active, posX-1, posY)){
    posX-=1;
    return;
  }
}

//Thread safe.
void caerForzado(){
  while(!(esEstatico())){
    posY+=1;
  }
}

//Thread safe.
boolean esEstatico(){
  if(!colisiona(active, posX, posY+1)){
    return false;
  }
  return true;
}

//Thread safe.
void caer(){
  if(!esEstatico()){
    posY+=1;
  }
}

//Thread safe.
boolean enBorde(){
  if(colisiona(active, posX+1, posY) || colisiona(active, posX-1, posY)){
    return true;
  }
  return false;
}

//Thread unsafe.
void generarPieza(){
  int i = random(0,7);
  active = pzas[i];
  posX=4;
  posY=6;
  if(continuar()){
    return;
  }
  else{
    reset();
    generarPieza();
  }
}


//Thread unsafe.
void reset(){
  byte i=0;
  nvl = pntj = 0;
  while(i<16){
    screen[i]=0;
  }
}

//Thread "safe".
boolean continuar(){
  if(!colisiona(active, posX, posY)){
    return true;
  }
  return false;
}