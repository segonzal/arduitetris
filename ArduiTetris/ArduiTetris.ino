// Pins de la pantalla led
int latchPin = 11;
int clockPin = 13;
int dataPin = 12;

// Pins del control
int pinA = 9;
int pinB = 10;
int pinReset = 8;
int pinI = 6;
int pinD = 7;

//refreshing screen time
unsigned long time;

// Variables globales
byte posX, posY, nvl;
int active, pntj, lineas;
//O I S Z L J T
int pzas[] = {1632,240,1728,3168,736,2272,1248};
//pantalla
char screen[16];

//=================================================================================
//============================== CONTROL DE GRAFICOS ==============================
//=================================================================================
/**
  * Envia serialmente los nbits menos significativos de data.
  * int nbits, numero de bits.
  * int data, datos.
  */
void shift_Out(int nbits,int data) {
  for (int i=0;i<nbits;i++){
    int j = data & (1<<i);
    digitalWrite(clockPin, LOW);
    digitalWrite(dataPin, j!=0);
    digitalWrite(clockPin, HIGH);
  }
}

/**
  * Refresca la pantalla por dt milisegundos.
  * int dt, tiempo de duracion del refresco de pantalla en milisegundos.
  */
void actualizaGraficos(int dt) {
  unsigned long newtime;
  int x = 6-posX;
  int y = 16-posY;
  do{
    char pantalla[16];
    for(int i=0;i<16;i++){
      pantalla[i]=screen[i];
    }
    int t1 = (active & 0x000f)<<4;
    int t2 = (active & 0x00f0);
    int t3 = (active & 0x0f00)>>4;
    int t4 = (active & 0xf000)>>8;
    pantalla[y+1] |= x<0? t1<<-x : t1>>x;
    pantalla[y]   |= x<0? t2<<-x : t2>>x;
    pantalla[y-1] |= x<0? t3<<-x : t3>>x;
    pantalla[y-2] |= x<0? t4<<-x : t4>>x;
    for (int i=0;i<16;i++){
      digitalWrite(latchPin, LOW);
      shift_Out(16,1<<i);
      shift_Out(8, pantalla[i]);
      digitalWrite(latchPin, HIGH);
    }
    newtime = millis();
  }while((newtime-time) < dt);
  time = newtime;
}

//=================================================================================
//============================ CONTROL DE PIEZA ACTIVA ============================
//=================================================================================
/**
  * Verifica si la pieza dada como argumento colisiona dada su posicion.
  * int pieza, pieza a verificar colision.
  * byte x, posicion de la pieza en el eje x.
  * byte y, posicion de la pieza en el eje y.
  */
boolean colisiona(int pieza, byte x, byte y){
  int _x = 6-x;
  int _y = 16-y;
  int s0,s1,s2,s3;  
  int t1 = (active & 0x000f)<<4;
  int t2 = (active & 0x00f0);
  int t3 = (active & 0x0f00)>>4;
  int t4 = (active & 0xf000)>>8;
  if (t4!=0 && _y==1) return true;
  else if (t3!=0 && _y==0) return true;
  else if (t2!=0 && _y==-1) return true;
  else if (t1!=0 && _y==-2) return true;
  s0 = screen[_y+1] & (_x<0? t1<<-_x : t1>>_x);
  s1 = screen[_y]   & (_x<0? t2<<-_x : t2>>_x);
  s2 = screen[_y-1] & (_x<0? t3<<-_x : t3>>_x);
  s3 = screen[_y-2] & (_x<0? t4<<-_x : t4>>_x);
  return s3!=0 || s2!=0 || s1!=0 || s0!=0;
}

/**
  * Rota la pieza activa.
  * Modifica active.
  */
void rotar(){
  int newactive = 0;
  switch(active){
    case 1632:
      break;
    case 0x08c4:
      newactive=1728;
      break;
    case 0x04c8:
      newactive=3168;
      break;
    default:
      int j=2;
      for (int i=0;i<16;i++){
        boolean value = (active & (1<<i))!=0;
        int index = 15 - (4*(i%4)+j);
        bitWrite(newactive,index,value);
        if(i==3 || i==7) j--;
        if(i==11) j=3;
      }
  }
  if(!colisiona(newactive,posX,posY)){
    active = newactive;
  }
}

/**
  * Mueve la pieza a la Izquierda.
  * Modifica posX.
  */
void moverI(){
  if(!colisiona(active, posX, posY+1) && !colisiona(active, posX-1, posY) && (posX>2 || !enBorde())){
    posX--;
  }
}

/**
  * Mueve la pieza a la Derecha.
  * Modifica posX.
  */
void moverD(){
  if(!colisiona(active, posX, posY+1) && !colisiona(active, posX+1, posY) && (posX<6 || !enBorde())){
    posX++;
  }
}

/**
  * Deja caer la pieza hasta colisionar.
  * Modifica posY.
  */
void caerForzado(){
  while( !esEstatico()){
    caer();
  }
}

/**
  * Verifica si la pieza puede ser movida hacia abajo, ie no colisiona inferiormente.
  * retorna True Si la pieza colisiona inferiormente, False eoc.
  */
boolean esEstatico(){
  return colisiona(active, posX, posY+1);
}

/**
  * Verifica si la pieza puede ser movida hacia los lados.
  * retorna True Si la pieza colisiona con los bordes, False eoc.
  */
boolean enBorde(){
  switch(posX){
    case 1:
      return (active & 0x2222 )!=0;
    case 2:
      return (active & 0x1111 )!=0;
    case 6:
      return (active & 0x8888 )!=0;
    case 0:
    case 7:
      return true;
    default:
      return false;
    }
}

/**
  * Mueve la pieza una posicion hacia abajo, solo si no colisiona.
  * Modifica posY.
  */
void caer(){
  if(!esEstatico()){
    posY+=1;
  }
}

//=================================================================================
//=========================== CONTROL DE PIEZA ESTATICA ===========================
//=================================================================================
/**
  * Detecta la lineas completas, las borra y deja caer la pieza estatica.
  * Modifica screen.
  * retorna el numero de lineas borradas.
  */
int actualizaLinea(){
  int i, p=0;
  boolean run;
  char b;
  for(i=0; i<=15; i++){
    b=1;
    for(int j=0;j<8;j++){
      b&=bitRead(screen[i],j);
    }
    if(b!=0){bajarLinea(i+1); p++; i--;}
  }
  return p;
}

/**
  * Deja caer las lineas desde k hacia arriba.
  * Modifica screen.
  */
void bajarLinea(int k){ //baja un espacio todos los bits de la pieza estatica a partir de la linea k hacia arriba
  int i;
  for (i=k; i<=15; i++){
    screen[i-1]=screen[i];
  }
  screen[15]=0x00;
}

/**
  * Agrega la pieza activa a la pieza estatica en la posicion actual.
  * Setea la pieza activa en 0, y deja posX y posY en el punto de spawn.
  * Modifica active.
  * Modifica screen.
  */
void agregar(){
    int x = 6-posX;
    int y = 16-posY;
    
    int t1 = (active & 0x000f)<<4;
    int t2 = (active & 0x00f0);
    int t3 = (active & 0x0f00)>>4;
    int t4 = (active & 0xf000)>>8;
    screen[y+1] |= x<0? t1<<-x : t1>>x;
    screen[y]   |= x<0? t2<<-x : t2>>x;
    screen[y-1] |= x<0? t3<<-x : t3>>x;
    screen[y-2] |= x<0? t4<<-x : t4>>x;
    active=0;
    posY=0;
    posX=4;

}

/**
  * Detecta el input y llama al metodo de movimiento correspondiente.
  * retorna false si el boton reset fue presionado, true eoc.
  */
boolean actualizaInput(){
  if(!digitalRead(pinI) && digitalRead(pinD)){
    moverI();
  }
  if(!digitalRead(pinD) && digitalRead(pinI)){
    moverD();
  }
  if(!digitalRead(pinA)){
    rotar();
  }
  if(!digitalRead(pinB)){
    caerForzado();
  }
  return !digitalRead(pinReset);
}

//=================================================================================
//===================================== SPAWNER ===================================
//=================================================================================
/**
  * Elige aleatoriamente una pieza, actualiza active, resetea posX y posY a las posiciones iniciales.
  * Modifica active.
  * Modifica posX.
  * Modifica posY.
  */
void generarPieza(){
  int i = random(0,7);
  active = pzas[i];
  posX=4;
  posY=1;
}

/**
  * Verifica la condicion de GAME OVER, ie el bloque de spawn esta lleno.
  * retorna True si el juego continua, False en GAME OVER.
  */
boolean continuar(){
  return (screen[15] & 0x3c)==0 && (screen[14] & 0x3c)==0;
}

/**
  * Regresa el juego al estado inicial.
  * Modifica screen.
  * Modifica posX.
  * Modifica posY.
  * Modifica active.
  * Modifica nvl.
  * Modifica pntj.
  */
void reset(){
  byte i=0;
  pntj = 0;
  while(i<16){
    screen[i]=0;
    i++;
  }
  posX=0;
  posY=0;
  active=0;
  nvl=1;
}
//=================================================================================
//============================== ANIMACIONES & SONIDOS ============================
//=================================================================================
void animation_gameover() {
  while(digitalRead(pinReset)){
    for(int i=0;i<8;i++){
      screen[i]=0xff;
      screen[15-i]=0xff;
      actualizaGraficos(50);
    }
    for(int i=0;i<8;i++){
      screen[i]=0;
      screen[15-i]=0;
      actualizaGraficos(50);
    }
  }
  //esto evita entrar a un nuevo ciclo de endgame
  while(!digitalRead(pinReset));
}

//=================================================================================
//================================== SETUP & LOOP =================================
//=================================================================================
void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  pinMode(pinA, INPUT);
  pinMode(pinB, INPUT);
  pinMode(pinReset, INPUT);
  pinMode(pinI, INPUT);
  pinMode(pinD, INPUT);
}

void loop() {
  animation_gameover();
  time = millis();
  unsigned long countdown = 0;
  int delta;
  boolean run = true;
  reset();
  while(run){
    run = !actualizaInput();
    if ( active!=0 ){
        if( esEstatico()){
          agregar();
          int l = actualizaLinea();
          lineas += l;
          pntj += ((11-nvl)*24)*l;
        }else{
          caer();
        }
      }else{
        if ( continuar() ){
          generarPieza();
        }else{
          run=false;
        }
      }
    if ((11-nvl)*15>lineas && nvl<9){
      nvl++;
    }
    unsigned long newtime = millis();
    delta = newtime-time;
    int countdown = ( countdown - delta );
    if( countdown<=0 ){
      countdown = (50*(11-nvl));
    }
    actualizaGraficos(countdown);
  }
}
